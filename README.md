# chrome-os-kernel-packages

This repo contains no code files -- it is used as a place to store built kernel artifacts.

The packages are self built by me and have no guarantees whether they will work properly or not.

The packages are in https://gitlab.com/blenderfox/chrome-os-kernel-packages/-/packages and each bundle has two RPMs, two DEBs, the kernel config used to compile the kernel, and the commit message from me.

You only need to use the RPM versions for RHEL, CentOS and derivatives; and the DEB version for Debian, Ubuntu, and derivatives.

Install both the headers and kernel packages and then use something like `grub-customizer` to add to your grub boot.
Fedora users may need to also include `GRUB_ENABLE_BLSCFG=false` in their `/etc/default/grub` file to enable grub-customizer to pick up the list properly.
